//
//  ViewController.swift
//  NewsMVC2
//
//  Created by Aya E  Mahmoud on 11/2/19.
//  Copyright © 2019 Aya E  Mahmoud. All rights reserved.
//

import UIKit
import PullToRefreshKit
import Windless
import Kingfisher

class NewsViewController: UIViewController {

    @IBOutlet weak var newsTableView: UITableView!
    var page = 1
    var articles = [Article]()
    var windlessCount = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        startWindless()
        initPullToRefresh()
        initLoadMore()
        getNews()
        // Do any additional setup after loading the view.
    }
    
    func registerCell()
    {
        newsTableView.register(UINib(nibName: "NewsCell", bundle: nil), forCellReuseIdentifier: "NewsCell")
    }
    
    func startWindless(){
        self.newsTableView.windless.apply {
                $0.beginTime = 0
                $0.pauseDuration = 1
                $0.duration = 1.5
                $0.animationLayerOpacity = 0.8
            }.start()
    }
    
    func initLoadMore(){
        self.newsTableView.configRefreshFooter(container: self){
            self.page += 1
            self.getNews()
        }
    }
    
    func initPullToRefresh(){
        self.newsTableView.configRefreshHeader(container: self){
            self.page = 1
            self.getNews()
        }
    }
    
    func getNews() {
        let newsRequest = NewsRequest(countryCode: "US", pageSize: 10, page: page)
        NewsService.getNews(newsRequest: newsRequest) { (articles, error) in
            self.newsTableView.switchRefreshFooter(to: .normal)
            self.newsTableView.switchRefreshHeader(to: .normal(.success, 0.0))
            self.newsTableView.windless.end()
            self.windlessCount = 0
            
            if error == nil {
                if self.page == 1{
                    self.articles = articles
                }else {
                    self.articles.append(contentsOf: articles)
                }
                self.newsTableView.reloadData()
            } else {
                let alert = UIAlertController(title: "Someting went wrong", message: "Error occured", preferredStyle: .alert)
                let action = UIAlertAction(title: "Dismiss", style: .destructive, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

}

extension NewsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if page == 1 && articles.count == 0 {
            return windlessCount
        }else {
            return articles.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as? NewsCell
        if articles.count > 0 {
            let article = articles[indexPath.row]
            cell?.newsTitle.text = article.title
            cell?.newsDescription.text = article.descriptionValue
            cell?.newsUrl.text = article.url
            if let imageString = article.urlToImage , let url = URL(string: imageString) {
                cell?.newsImage.kf.setImage(with: url)
            }
        }
        return cell!
    }
}

extension NewsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 200.00
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if articles.count > 0 {
            let newsDetail = articles[indexPath.row]
            self.performSegue(withIdentifier: "newsdetails", sender: newsDetail)
        }
    }
}
