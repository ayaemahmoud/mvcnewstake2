//
//  DetailsViewController.swift
//  NewsMVC2
//
//  Created by Aya E  Mahmoud on 11/10/19.
//  Copyright © 2019 Aya E  Mahmoud. All rights reserved.
//

import Foundation
import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsDetails: UILabel!
    var article: Article?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if let article = article {
            newsTitle.text = article.title
            newsDetails.text = article.descriptionValue
        }
        // Do any additional setup after loading the view.
    }
}

