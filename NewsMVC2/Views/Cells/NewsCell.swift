//
//  NewsCell.swift
//  NewsMVC2
//
//  Created by Aya E  Mahmoud on 11/2/19.
//  Copyright © 2019 Aya E  Mahmoud. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    @IBOutlet weak var newsDescription: UILabel!
    @IBOutlet weak var newsUrl: UILabel!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
