//
//  NewsRequest.swift
//  MVCNews
//
//  Created by Aya E  Mahmoud on 9/19/19.
//  Copyright © 2019 Aya E  Mahmoud. All rights reserved.
//

import Foundation

struct NewsRequest {
    let countryCode: String
    let pageSize: Int
    let page: Int
}
